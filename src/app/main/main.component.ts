import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})

export class MainComponent implements OnInit {

  bill = "";
  custom = "";
  nbrPeople = "";
  amountPerson = 0;
  total = 0;

  onInputBill(value: string){
    this.bill = value;
    console.log(this.bill);
  }
  onInputNbr(value: string){
    this.nbrPeople = value;
    console.log(this.nbrPeople);
  }
  onInputCustom(value: string){
    this.custom = value;
    console.log(this.custom);
  }

  constructor() {
    
  }
  ngOnInit(): void {
  }

  customValue(){
    this.resetCss()
    if(Number(this.custom) > 0 && Number(this.bill) > 0 && Number(this.nbrPeople) > 0){
      this.total = Number(this.bill)*Number(this.custom)/100
      this.amountPerson =  Math.round(this.total/Number(this.nbrPeople) * 100) / 100
    }else if(Number(this.bill) == 0 || Number(this.bill) <= 0){
      this.billCss()
    }else if(Number(this.nbrPeople) == 0 || Number(this.nbrPeople) <= 0){
      this.nbrPeopleCss()
    }else if(Number(this.custom) < 0){
      this.customCss()
    }
  }
  
  calcTip(percentage: number){
    this.resetCss()
    if(Number(this.custom) > 0){
      this.custom = ""
    }
    if(Number(this.bill) > 0 && Number(this.nbrPeople) > 0){
      this.total = Number(this.bill)*percentage/100
      this.amountPerson =  Math.round(this.total/Number(this.nbrPeople) * 100) / 100  
    }else if(Number(this.bill) == null || Number(this.bill) <= 0){
      this.billCss()
    }
    else if(this.nbrPeople == null || Number(this.nbrPeople) <= 0){
     this.nbrPeopleCss()
    }
  }

  billCss(){
    let inputBill = document.querySelector(".input-bill") as HTMLElement
    inputBill.style.borderColor = "red"
    document.querySelector<HTMLElement>(".span-bill")?.classList.add("error")
    let spanBillError = document.querySelector<HTMLElement>(".span-bill.error")
    if (spanBillError != null){
      spanBillError.innerHTML ="*Amount of bill incorrect"
    }
  }

  nbrPeopleCss(){
    let inputNbr = document.querySelector(".input-nbr") as HTMLElement
    inputNbr.style.borderColor = "red"
    document.querySelector<HTMLElement>(".span-nbr")?.classList.add("error")
    let spanNbrError = document.querySelector<HTMLElement>(".span-nbr.error")
    if (spanNbrError != null){
      spanNbrError.innerHTML ="*Total of people incorrect"
    }
  }

  customCss(){
    let inputCustom = document.querySelector(".inpucustom") as HTMLElement
    inputCustom.style.borderColor = "red"
    document.querySelector<HTMLElement>(".span-custom")?.classList.add("error")
    let spanCustomError = document.querySelector<HTMLElement>(".span-custom.error")
    if (spanCustomError != null){
      spanCustomError.innerHTML ="*Wrong amount of tip"
    }
  }

  reset(){
    this.resetCss();
    this.bill = "";
    this.custom = "";
    this.nbrPeople = "";
    this.amountPerson = 0;
    this.total = 0;
  }

  resetCss(){
    let inputBill = document.querySelector(".input-bill") as HTMLElement
    if (inputBill != null){
      inputBill.style.borderColor = ""
    }
    document.querySelector(".span-bill")?.classList.remove("error")
    let inputNbr = document.querySelector(".input-nbr") as HTMLElement
    if (inputNbr != null){
      inputNbr.style.borderColor = ""
    }
    document.querySelector(".span-nbr")?.classList.remove("error")
    let inputCustom = document.querySelector(".inpucustom") as HTMLElement
    if (inputCustom != null){
      inputCustom.style.borderColor = ""
    }
    document.querySelector(".span-custom")?.classList.remove("error")
  }
}
